package com.zero.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.GridView;

/**
 * Created by tengtao on 16/8/16.
 */
public class HorizontalGridView extends GridView {

    private int mRows = 2;
    private int mNumColumns = 4;
    private int mScreenWidth;

    public HorizontalGridView(Context context) {
        this(context, null);
    }

    public HorizontalGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HorizontalGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mScreenWidth = context.getResources().getDisplayMetrics().widthPixels;
    }

    @Override
    public int getNumColumns() {
        int columns = super.getNumColumns();
        return columns <= 0 ? mNumColumns : columns;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int count = getChildCount();
        if (count == 0) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }
        int page = count / mRows / mNumColumns + 1;
        Log.e("debug","page="+page);
        setMeasuredDimension(mScreenWidth * page, MeasureSpec.getSize(heightMeasureSpec));

    }
}
