package com.zero.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.zero.activity.custom.CustomAcivity;
import com.zero.activity.glide.GlideActivity;
import com.zero.activity.gridview.HorizontalGridViewActivity;
import com.zero.activity.recyclerview.RecyclerViewActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.listview)
    ListView mListview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        String[] list = getResources().getStringArray(R.array.list);
        mListview.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list));
        mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Context context=MainActivity.this;
                switch (position){
                    case 0:
                        RecyclerViewActivity.launch(context);
                        break;
                    case 1:
                        CustomAcivity.launch(context);
                        break;
                    case 2:
                        HorizontalGridViewActivity.launch(context);
                        break ;
                    case 3:
                        StatusActivity.launch(context);
                        break;
                    case 4:
                        PropertyAnimationActivity.launch(context);
                        break ;
                    case 5:
                        GlideActivity.launch(context);
                        break;
                }

            }
        });
    }
}
