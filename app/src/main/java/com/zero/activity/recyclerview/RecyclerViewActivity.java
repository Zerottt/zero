package com.zero.activity.recyclerview;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;

import com.zero.activity.R;
import com.zero.activity.base.BaseActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.recyclerview.adapters.SlideInBottomAnimationAdapter;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import jp.wasabeef.recyclerview.animators.holder.AnimateViewHolder;

/**
 * Created by tengtao on 16/8/10.
 */
public class RecyclerViewActivity extends BaseActivity {


    @BindView(R.id.recycler)
    RecyclerView mRecyclerView;
    private ArrayList<String> mDatas;
    private HomeAdapter mAdapter;

    public static void launch(Context context) {
        context.startActivity(new Intent(context, RecyclerViewActivity.class));
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recylerview);
        ButterKnife.bind(this);
        setToolBar();
        initData();


//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        GridLayoutManager manager = new GridLayoutManager(this, 2);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerView.setLayoutManager(manager);

        SlideInBottomAnimationAdapter scaleAdapter = new SlideInBottomAnimationAdapter(mAdapter = new HomeAdapter(this));
        scaleAdapter.setFirstOnly(false);
        mRecyclerView.setAdapter(scaleAdapter);

        mRecyclerView.setItemAnimator(new SlideInUpAnimator(new OvershootInterpolator(1f)));

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.recycler, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.id_action_add:
                addData(2);
                break;
            case R.id.id_action_delete:
                removeData(2);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void initData() {
        mDatas = new ArrayList<String>();
        for (int i = 'A'; i < 'x'; i++) {
            mDatas.add("" + (char) i);
        }
    }

    public void addData(int position) {
        mDatas.add(position, "Insert One");
        mAdapter.notifyItemInserted(position);
    }

    public void removeData(int position) {
        mDatas.remove(position);
        mAdapter.notifyItemRemoved(position);
    }

    @Override
    public void setToolBar() {
        super.setToolBar();
        setTitle("RecyclerView");
    }

    class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

        private int mScreenWidth;

        public HomeAdapter(Context context) {
            mScreenWidth = context.getResources().getDisplayMetrics().widthPixels;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            MyViewHolder holder = new MyViewHolder(LayoutInflater.from(
                    RecyclerViewActivity.this).inflate(R.layout.item_home, parent,
                    false));
            return holder;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            holder.tv.setText(mDatas.get(position));
        }

        @Override
        public int getItemCount() {
            return mDatas.size();
        }

        class MyViewHolder extends AnimateViewHolder {

            TextView tv;

            public MyViewHolder(View view) {
                super(view);
//                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) view.getLayoutParams();
//                params.width = mScreenWidth / 4;
//                view.setLayoutParams(params);
                tv = (TextView) view.findViewById(R.id.id_num);
            }

            @Override
            public void animateRemoveImpl(ViewPropertyAnimatorListener listener) {
                ViewCompat.animate(itemView)
                        .translationY(-itemView.getHeight() * 0.3f)
                        .alpha(0)
                        .setDuration(300)
                        .setListener(listener)
                        .start();
            }

            @Override
            public void preAnimateAddImpl() {
                ViewCompat.setTranslationY(itemView, -itemView.getHeight() * 0.3f);
                ViewCompat.setAlpha(itemView, 0);
            }

            @Override
            public void animateAddImpl(ViewPropertyAnimatorListener listener) {
                ViewCompat.animate(itemView)
                        .translationY(0)
                        .alpha(1)
                        .setDuration(300)
                        .setListener(listener)
                        .start();
            }
        }
    }


}
