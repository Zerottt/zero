package com.zero.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tengtao on 16/8/17.
 */
public class PropertyAnimationActivity extends AppCompatActivity {
    @BindView(R.id.id_tool_bar)
    Toolbar mIdToolBar;
    @BindView(R.id.id_ball)
    ImageView mIdBall;
    @BindView(R.id.title)
    TextView mTitle;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_animation);
        ButterKnife.bind(this);
        setSupportActionBar(mIdToolBar);
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, PropertyAnimationActivity.class));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_property_anim, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_rotationX:
                rotateyXAnimRun();
                break;
            case R.id.menu_more_anim:
                moreAnimRun();
                break;
            case R.id.menu_TranslationY:
                TranslationYAnimRun();
                break;
            case R.id.menu_scale:
                scaleAnimRun();
                break;
        }
        return true;
    }

    private void scaleAnimRun() {

        ObjectAnimator animator = ObjectAnimator.ofFloat(mIdBall, "eeeee", 0.0f, 1.1f, 1f).setDuration(500);
        animator.start();
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value = (float) valueAnimator.getAnimatedValue();
                mIdBall.setScaleX(value);
                mIdBall.setScaleY(value);
            }
        });
    }

    private void moreAnimRun() {
        ObjectAnimator animator = ObjectAnimator.ofFloat(mIdBall, "sdavw", 1.0f, 0.0f, 1.0f).setDuration(1000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value = (float) valueAnimator.getAnimatedValue();
                mIdBall.setAlpha(value);
                mIdBall.setScaleX(value);
                mIdBall.setScaleY(value);
                mIdBall.setRotation(360 * value);
            }
        });
        animator.start();
    }

    public void rotateyXAnimRun() {
        ObjectAnimator.ofFloat(mIdBall, "RotationX", 0, 360f).setDuration(300).start();
    }

    public void TranslationYAnimRun() {
//        ObjectAnimator.ofFloat(mIdBall, "TranslationY", 0, -300).setDuration(300).start();
        mTitle.bringToFront();
        mIdBall.setTranslationY(-300);
    }
}
