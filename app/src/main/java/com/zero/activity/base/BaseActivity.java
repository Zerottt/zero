package com.zero.activity.base;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.zero.activity.R;

/**
 * Created by tengtao on 16/8/10.
 */
public class BaseActivity extends AppCompatActivity {

    protected Toolbar mToolbar;

    public void setToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.id_tool_bar);
        setSupportActionBar(mToolbar);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
