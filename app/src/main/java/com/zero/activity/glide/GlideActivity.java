package com.zero.activity.glide;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import com.zero.activity.R;

/**
 * Created by tengtao on 16/8/24.
 */
public class GlideActivity extends AppCompatActivity {
    private ImageView mImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glide);

        mImage = (ImageView) findViewById(R.id.image);

    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, GlideActivity.class));
    }

    public void loadImgClick(View v) {
        CropCircleBorderTransformation transformation = new CropCircleBorderTransformation(this).setBorderWidth(10);
        Glide.with(this).load(R.drawable.dd).placeholder(R.mipmap.ic_logo).crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)//让Glide既缓存全尺寸又缓存其他尺寸
                .bitmapTransform(transformation).into(mImage);

    }

    public void clearClick(View view) {
        Glide.clear(mImage);
    }

    class CropCircleBorderTransformation implements Transformation<Bitmap> {


        private BitmapPool mBitmapPool;
        private int mBorderColor = Color.WHITE;
        private float mBorderWidth;

        public CropCircleBorderTransformation(Context context) {
            this(Glide.get(context).getBitmapPool());
        }

        public CropCircleBorderTransformation(BitmapPool pool) {
            this.mBitmapPool = pool;
        }

        public CropCircleBorderTransformation setBorderColor(@ColorInt int borderColor) {
            mBorderColor = borderColor;
            return this;
        }

        public CropCircleBorderTransformation setBorderWidth(float borderWidth) {
            mBorderWidth = borderWidth;
            return this;
        }

        public void setBorder(@ColorInt int borderColor, float borderWidth) {
            mBorderColor = borderColor;
            mBorderWidth = borderWidth;
        }

        @Override
        public Resource<Bitmap> transform(Resource<Bitmap> resource, int outWidth, int outHeight) {
            Bitmap source = resource.get();
            int size = Math.min(source.getWidth(), source.getHeight());

            int width = (source.getWidth() - size) / 2;
            int height = (source.getHeight() - size) / 2;

            Bitmap bitmap = mBitmapPool.get(size, size, Bitmap.Config.ARGB_8888);
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader =
                    new BitmapShader(source, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            if (width != 0 || height != 0) {
                // source isn't square, move viewport to center
                Matrix matrix = new Matrix();
                matrix.setTranslate(-width, -height);
                shader.setLocalMatrix(matrix);
            }
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;


            if (mBorderWidth > 0) {
                Paint borderPaint = new Paint();
                borderPaint.setAntiAlias(true);
                borderPaint.setColor(mBorderColor);

                canvas.drawCircle(r, r, r, borderPaint);
            }
            canvas.drawCircle(r, r, r - mBorderWidth, paint);

            return BitmapResource.obtain(bitmap, mBitmapPool);
        }

        @Override
        public String getId() {
            return "CropCircleBorderTransformation()";
        }
    }
}
