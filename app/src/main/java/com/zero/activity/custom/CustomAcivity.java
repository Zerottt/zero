package com.zero.activity.custom;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.zero.activity.R;
import com.zero.activity.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tengtao on 16/8/11.
 */
public class CustomAcivity extends BaseActivity {

    @BindView(R.id.listview)
    ListView mListview;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setToolBar();
        String[] list = getResources().getStringArray(R.array.CustomView);
        mListview.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list));
        mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Context context = CustomAcivity.this;
                switch (position) {
                    case 0:
                        WatchBoardActivity.launch(context);
                        break;
                    case 1:
                        CustomAcivity.launch(context);
                        break;
                }

            }
        });
    }

    @Override
    public void setToolBar() {
        super.setToolBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public static void launch(Context context) {

        context.startActivity(new Intent(context, CustomAcivity.class));
    }
}
