package com.zero.activity.custom;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.zero.activity.R;
import com.zero.activity.base.BaseActivity;

/**
 * Created by tengtao on 16/8/11.
 */
public class WatchBoardActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch_board);
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, WatchBoardActivity.class));
    }
}
