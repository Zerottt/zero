package com.zero.activity.gridview;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import com.zero.activity.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tengtao on 16/8/16.
 */
public class HorizontalGridViewActivity extends AppCompatActivity {

    @BindView(R.id.gridview)
    GridView mGridview;
    private List<String> mStringList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horizontal_gridview);
        ButterKnife.bind(this);
        initDatas();
        mGridview.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mStringList));
    }

    private void initDatas() {
        for (int i = 0; i < 20; i++) {
            mStringList.add("" + (i + 1));
        }
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, HorizontalGridViewActivity.class));
    }
}
