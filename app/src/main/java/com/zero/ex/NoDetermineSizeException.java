package com.zero.ex;

/**
 * Created by tengtao on 16/8/11.
 */
public class NoDetermineSizeException extends Exception {
    public NoDetermineSizeException(String message) {
        super(message);
    }
}
