package com.zero.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.ListView;

/**
 * Created by tengtao on 16/8/1.
 */
public class CoustomListView extends ListView {
    private final int mTouchSlop;
    private float mInitialMotionX;
    private float mInitialMotionY;

    public CoustomListView(Context context) {
        this(context, null);
    }

    public CoustomListView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CoustomListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
//        Log.e("debug", "hhhhhhh");

        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        Log.e("debug", "eeeeeeeeeeee");

        final float x = ev.getX();
        final float y = ev.getY();
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mInitialMotionX = x;
                mInitialMotionY = y;

                break;
            case MotionEvent.ACTION_MOVE:

                final float adx = Math.abs(x - mInitialMotionX);
                final float ady = Math.abs(y - mInitialMotionY);
                if (ady > mTouchSlop && adx < ady) {
                    getParent().requestDisallowInterceptTouchEvent(true);
                }else{
                    getParent().requestDisallowInterceptTouchEvent(false);
                }
                break;

        }
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        return super.onTouchEvent(ev);
    }
}
