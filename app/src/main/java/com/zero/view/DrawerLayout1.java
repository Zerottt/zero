package com.zero.view;

import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by tengtao on 16/8/2.
 */
public class DrawerLayout1 extends DrawerLayout {

    private int mScreenWidth;

    public DrawerLayout1(Context context) {
        this(context, null);
    }

    public DrawerLayout1(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DrawerLayout1(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mScreenWidth = getResources().getDisplayMetrics().widthPixels;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        measureChild(getChildAt(0), widthMeasureSpec, heightMeasureSpec);
//        super.onMeasure(getChildAt(0).getMeasuredWidth() + mScreenWidth + MeasureSpec.EXACTLY, heightMeasureSpec);
        setMeasuredDimension(getChildAt(0).getMeasuredWidth() + mScreenWidth + MeasureSpec.EXACTLY,heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (changed) {
            View contentView = getChildAt(0);
            contentView.layout(0, t, contentView.getMeasuredWidth(), contentView.getMeasuredHeight());
            View menuView = getChildAt(1);
            menuView.layout(contentView.getMeasuredWidth(), t, contentView.getMeasuredWidth() + menuView.getMeasuredWidth(), menuView.getMeasuredHeight());
        }
    }
}
