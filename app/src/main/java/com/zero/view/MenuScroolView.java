package com.zero.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;

/**
 * Created by tengtao on 16/8/2.
 */
public class MenuScroolView extends FrameLayout {
    private final GestureDetectorCompat gestureDetector;
    private View mDragView;
    private boolean isOpen;
    private int mMaxTranslationX;

    private int mScreenWidth;

    private static final int MAX_OFFSET = 5;// 5个像素误差，滑动小于5个像素就没有动画
    private float downX;// 按下时的点
    private float viewXdown;// 按下时View的位置
    private boolean lastSlidePull = false;// 最后一次滑动的方向
    private float maxOffset = 0;// 最大的滑动距离
    private int mLastMotionX;

    public MenuScroolView(Context context) {
        this(context, null);
    }

    public MenuScroolView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MenuScroolView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mScreenWidth = getResources().getDisplayMetrics().widthPixels;
        gestureDetector = new GestureDetectorCompat(context, new YScrollDetector());
    }


    public void open() {
        if (!isOpen) {
            isOpen = true;
            ObjectAnimator animator = ObjectAnimator.ofFloat(mDragView, "TranslationX", 0f, mMaxTranslationX);
            animator.setDuration(500);
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.start();
        }
    }

    public void close() {
        if (isOpen) {
            isOpen = false;
            ObjectAnimator animator = ObjectAnimator.ofFloat(mDragView, "TranslationX", mMaxTranslationX, 0);
            animator.setDuration(500);
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.start();
        }
    }

    class YScrollDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float dx, float dy) {
            return Math.abs(dx) >= Math.abs(dy);
        }
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        measureChild(getChildAt(0), widthMeasureSpec, heightMeasureSpec);
        super.onMeasure(getChildAt(0).getMeasuredWidth() + mScreenWidth + MeasureSpec.EXACTLY, heightMeasureSpec);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mMaxTranslationX = getChildAt(0).getMeasuredWidth();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mDragView = getChildAt(1);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return gestureDetector.onTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                mLastMotionX = x;
                break;
            case MotionEvent.ACTION_UP:

                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:

                moveViewByLayout(mDragView, (int) event.getRawX(), 0);


                int left = mDragView.getLeft();
                int dx = (int) (getX() - mLastMotionX);
                mDragView.offsetLeftAndRight(dx);


//
//                Log.e("debug","left="+left);
//                int dx = x - mLastX;
//                往左滑动
//
//
//                mDragView.setTranslationX(dx);
//                mDragView.setX();
//                mLastX = x;

//                float offset = (event.getX() - downX);// 滑动距离
//                int left = mDragView.getLeft();
//                if ((offset < 0 && left <= 0) || (offset > 0 && left >= mMaxTranslationX)) {
//                    return false;
//                }

//                mDragView.offsetLeftAndRight((int) offset);
//                if (offset > 0) {// pull to show
//                    lastSlidePull = true;
//                } else {// push to hide
//                    lastSlidePull = false;
//                }
                break;
        }
        return super.onTouchEvent(event);
    }

    private void moveViewByLayout(View view, int rawX, int rawY) {
        int left = rawX - this.getWidth();
        int top = rawY - getHeight();
        int width = left + view.getWidth();
        int height = top + view.getHeight();
        view.layout(left, top, width, height);
    }
}
