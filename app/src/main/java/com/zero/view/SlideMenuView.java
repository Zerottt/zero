package com.zero.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import com.nineoldandroids.view.ViewHelper;
import com.zero.activity.R;

/**
 * Created by tengtao on 16/8/3.
 */
public class SlideMenuView extends HorizontalScrollView {
    private final GestureDetectorCompat gestureDetector;
    private int mScreenWidth;
    private ViewGroup mParentView;
    private View mLeftMenuView;
    private View mTopMenuView;
    private View mContentView;
    private boolean isOnce;

    private int mMenuWidth = 124;
    private boolean isOpen;


    private OnMenuScrollListener mOnMenuScrollListener;
    private OnMenuStatusChangeListener mOnMenuStatusChangeListener;

    private GestureDetectorCompat mGestureDetectorCompat;

    public SlideMenuView(Context context) {
        this(context, null);
    }

    public SlideMenuView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SlideMenuView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SlidingMenu, defStyleAttr, 0);

        int defaultMenuWidth = dip2px(context, mMenuWidth);
        mMenuWidth = a.getDimensionPixelSize(R.styleable.SlidingMenu_menu_width, defaultMenuWidth);
        a.recycle();


        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(outMetrics);
        mScreenWidth = outMetrics.widthPixels;

        gestureDetector = new GestureDetectorCompat(context, new YScrollDetector());

    }


    class YScrollDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float dx, float dy) {
            return Math.abs(dy) <= Math.abs(dx);
        }
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mParentView = (ViewGroup) getChildAt(0);
//        mLeftMenuView = findViewById(R.id.left_menu);
//        mContentView = findViewById(R.id.view_content);
//        mTopMenuView = findViewById(R.id.top_menu);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (!isOnce) {
            mTopMenuView.getLayoutParams().width = mScreenWidth;
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mContentView.getLayoutParams();
            params.width = mScreenWidth - mMenuWidth;
            params.rightMargin = mMenuWidth;

            ViewHelper.setTranslationX(mTopMenuView, -mMenuWidth);
            isOnce = true;
        }


        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if (changed) {
//            this.scrollTo(mMenuWidth, 0);
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        return super.onInterceptTouchEvent(ev) && gestureDetector.onTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_UP:
                int scrollX = getScrollX();

                if (scrollX >= mMenuWidth / 2) {
                    this.smoothScrollTo(mMenuWidth, 0);
                    isOpen = false;
                    menuStatusChange();
                } else {
                    this.smoothScrollTo(0, 0);
                    isOpen = true;
                    menuStatusChange();
                }
                return true;
        }
        return super.onTouchEvent(ev);
    }

    public void openMenu() {
        if (!isOpen) {
            isOpen = true;
            smoothScrollTo(0, 0);
            menuStatusChange();
        }
    }

    public void closeMenu() {
        if (isOpen) {
            isOpen = false;
            smoothScrollTo(mMenuWidth, 0);
            menuStatusChange();
        }
    }

    public void toggle() {
        if (isOpen) {
            closeMenu();
        } else {
            openMenu();
        }
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        //向右滑动为1~0，向左滑动为0~1
        float scale = l * 1.0f / mMenuWidth;//1-0

        float leftAlpha = 0.6f + 0.4f * (1 - scale);


        ViewHelper.setTranslationX(mLeftMenuView, mMenuWidth * scale * 0.8f);
        ViewHelper.setAlpha(mLeftMenuView, leftAlpha);


//        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mTopMenuView.getLayoutParams();
//        params.leftMargin = (int) (mMenuWidth * (1-scale));
//        params.rightMargin = (int) (mScreenWidth + (mMenuWidth * (1-scale)));
//        params.width = (int) (mScreenWidth - mMenuWidth * (scale));
//        mTopMenuView.setLayoutParams(params);


//        ViewHelper.setTranslationX(mTopMenuView, mMenuWidth * (scale));


        if (mOnMenuScrollListener != null) {
            mOnMenuScrollListener.onMenuScroll(scale);
        }

    }

    private void menuStatusChange() {
        if (mOnMenuStatusChangeListener != null) {
            mOnMenuStatusChangeListener.onMenuStatus(isOpen);
        }


        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mContentView.getLayoutParams();
        if (isOpen) {
            params.width = mScreenWidth - mMenuWidth;
            params.rightMargin = mMenuWidth;
        } else {
            params.width = mScreenWidth;
            params.rightMargin = 0;
        }
        mContentView.setLayoutParams(params);

    }


    public void setOnMenuScrollListener(OnMenuScrollListener onMenuScrollListener) {
        mOnMenuScrollListener = onMenuScrollListener;
    }

    public void setOnMenuStatusChangeListener(OnMenuStatusChangeListener onMenuStatusChangeListener) {
        mOnMenuStatusChangeListener = onMenuStatusChangeListener;
    }

    public boolean isMenuOpen() {
        return isOpen;
    }

    public interface OnMenuScrollListener {
        /**
         * 滑动变化率
         *
         * @param scale open为1~0 ,close为0~1
         */
        void onMenuScroll(float scale);
    }

    public interface OnMenuStatusChangeListener {
        void onMenuStatus(boolean isOpen);
    }

    public static int dip2px(Context context, int dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}

