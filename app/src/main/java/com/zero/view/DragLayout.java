package com.zero.view;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by tengtao on 16/8/1.
 */
public class DragLayout extends LinearLayout {

    private ViewDragHelper mDragHelper;
    private GestureDetectorCompat gestureDetector;
    private View mDragView;

    private int mLeft;

    private int mScreenWidth;

    public DragLayout(Context context) {
        this(context, null);
    }

    public DragLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DragLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mScreenWidth = getResources().getDisplayMetrics().widthPixels;
        gestureDetector = new GestureDetectorCompat(context, new YScrollDetector());

        mDragHelper = ViewDragHelper.create(this, 1.0f, new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(View child, int pointerId) {
                return child == mDragView;
            }

            @Override
            public int clampViewPositionHorizontal(View child, int left, int dx) {
                final int leftBound = getPaddingLeft();
                final int rightBound = getWidth() - mDragView.getWidth();
                final int newLeft = Math.min(Math.max(left, leftBound), rightBound);
                return newLeft;
            }

            @Override
            public int clampViewPositionVertical(View child, int top, int dy) {
                return 0;
            }

            @Override
            public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {
                super.onViewPositionChanged(changedView, left, top, dx, dy);
                mLeft = left;
            }


            @Override
            public void onViewReleased(View releasedChild, float xvel, float yvel) {
                super.onViewReleased(releasedChild, xvel, yvel);

                int width = getChildAt(0).getMeasuredWidth();
                int left = width - mLeft < width / 2 ? width : 0;
//                mDragHelper.settleCapturedViewAt(left, 0);
//                postInvalidate();
                mDragHelper.smoothSlideViewTo(mDragView,left,0);
                postInvalidate();
            }

            @Override
            public int getViewVerticalDragRange(View child) {
                return -1;
            }
        });
    }

    class YScrollDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float dx, float dy) {
            return Math.abs(dx) >= Math.abs(dy);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        measureChild(getChildAt(0), widthMeasureSpec, heightMeasureSpec);
        super.onMeasure(getChildAt(0).getMeasuredWidth() + mScreenWidth + MeasureSpec.EXACTLY, heightMeasureSpec);


    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
//        mDragView = findViewById(R.id.view_content);

    }


    @Override
    //在diapatch中进行相应的事件的拦截
    public boolean dispatchTouchEvent(MotionEvent event) {
        final int action = MotionEventCompat.getActionMasked(event);
        switch (action) {
            case MotionEvent.ACTION_DOWN: {
                //传入初始值，否则，直接传入ACTION_MOVE，会导致NullPointError
                mDragHelper.processTouchEvent(event);
                break;
            }
            //处理多指触控
            case MotionEvent.ACTION_POINTER_DOWN: {
                //传入初始值，否则，直接传入ACTION_MOVE，会导致NullPointError
                mDragHelper.processTouchEvent(event);
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                //到顶部，并且是向下拉
                if (gestureDetector.onTouchEvent(event)) {
                    mDragHelper.processTouchEvent(event);
                    //发送cancel事件，防止listview响应之前的事件，出现点击操作。
                    event.setAction(MotionEvent.ACTION_CANCEL);
                    super.dispatchTouchEvent(event);
                    return true;
                }
                break;
            }
            case MotionEvent.ACTION_UP: {
                mDragHelper.processTouchEvent(event);
                break;

            }

            case MotionEvent.ACTION_CANCEL: {

                break;

            }

            default:
                break;
        }

        return super.dispatchTouchEvent(event);

    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        //不对事件做任何拦截
        return false;
    }


    @Override
    public void computeScroll() {
        super.computeScroll();
        if (mDragHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }
}
