package zero.news.mvp.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import zero.news.mvp.ui.activity.R;

/**
 * Created by tengtao on 2016/9/23.
 */

public class NewsFragment extends BaseFragment {
//    @BindView(R.id.listView)
//    ListView mListView;

    public static NewsFragment newInstance() {
        return new NewsFragment();
    }

    private List<String> mStringList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        for (int i = 0; i < 50; i++) {
            mStringList.add("新闻" + i);

        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, null);
        ButterKnife.bind(this, view);
//        setAdapter(mListView);
        return view;
    }

    public void setAdapter(ListView listView) {
        listView.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, mStringList));
    }
}
