package zero.news.mvp.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import zero.news.mvp.ui.fragment.NewsFragmentPagerAdapter;

/**
 * Created by tengtao on 2016/9/23.
 */

public class CoordinatorActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tabs)
    TabLayout mTabs;
    @BindView(R.id.appbar)
    AppBarLayout mAppbar;
    @BindView(R.id.viewpager)
    ViewPager mViewpager;
    @BindView(R.id.fab)
    FloatingActionButton mFab;
    @BindView(R.id.main_content)
    CoordinatorLayout mMainContent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_news1);
        ButterKnife.bind(this);
        setupToolbar();
        setupTabLayout();
        setupViewPaper();
    }

    private void setupTabLayout() {
        mTabs.setupWithViewPager(mViewpager);
    }

    private void setupViewPaper() {
        NewsFragmentPagerAdapter pagerAdapter = new NewsFragmentPagerAdapter(getSupportFragmentManager());
        mViewpager.setAdapter(pagerAdapter);
    }

    private void setupToolbar() {
//        setSupportActionBar(mToolbar);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, mDrawerLayout, mToolbar, 0, 0);
//        mDrawerLayout.addDrawerListener(toggle);
//        toggle.syncState();
    }
}
