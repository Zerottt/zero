package zero.news.mvp.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.CompoundButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import zero.news.mvp.ui.fragment.NewsFragmentPagerAdapter;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.nav_view)
    NavigationView mNavView;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tabs)
    TabLayout mTabs;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(0, R.anim.fade_out);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupDrawerContent();
        setupToolbar();
        setupViewPaper();
        setupTabLayout();
        initNightModeSwitch();


    }

    private void setupViewPaper() {
        NewsFragmentPagerAdapter pagerAdapter = new NewsFragmentPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(pagerAdapter);
    }

    private void setupTabLayout() {
        mTabs.setupWithViewPager(mViewPager);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, 0, 0);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void initNightModeSwitch() {
        MenuItem item = mNavView.getMenu().findItem(R.id.nav_night_mode);
        SwitchCompat switchCompat = (SwitchCompat) MenuItemCompat.getActionView(item);
        switchCompat.setChecked(true);
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            }
        });
    }

    private void setupDrawerContent() {
        mNavView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                item.setChecked(true);
                mDrawerLayout.closeDrawers();
                switch (item.getItemId()) {
                    case R.id.nav_news:
                        break;
                    case R.id.nav_photo:
                        break;
                    case R.id.nav_video:
                        break;
                }
                return true;
            }
        });
    }


    public static void launch(Context context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }
}
