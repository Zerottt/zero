package zero.news.mvp.ui.activity;

import android.app.Application;

import com.orhanobut.logger.Logger;

/**
 * Created by tengtao on 2016/9/23.
 */

public class App extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        Logger.init("debug");
    }
}

