package zero.vga;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * Created by tengtao on 16/9/8.
 */
public class OverScrollListView extends ListView {

    private int mMaxOverDistance = 200;

    public OverScrollListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        float density = getResources().getDisplayMetrics().density;
        mMaxOverDistance = (int) (mMaxOverDistance * density);
    }

    @Override
    protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX, int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {
        return super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX, scrollRangeY, maxOverScrollX,
                mMaxOverDistance, isTouchEvent);
    }
}
