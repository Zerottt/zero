package zero.vga;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by tengtao on 16/9/20.
 */
public class SmartButton extends View {
    private float mCenterX;
    private float mCenterY;
    private Paint mBackgroundPaint;
    private int mBackgroundColor = Color.parseColor("#b4282d");
    private int mShadowColor = Color.parseColor("#40000000");
    private Paint mPaint;
    private int mDotColor = Color.WHITE;
    private float mMinRadius = 6;

    public SmartButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(mDotColor);

        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setAntiAlias(true);
        mBackgroundPaint.setDither(true);
        mBackgroundPaint.setColor(mBackgroundColor);
        mBackgroundPaint.setShadowLayer(150, 0, 0, mShadowColor);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int size = MeasureSpec.getSize(widthMeasureSpec);
        setMeasuredDimension(size, size);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mCenterX = getMeasuredWidth() / 2.0f;
        mCenterY = getMeasuredHeight() / 2.0f;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawBackground(canvas);
        drawCenter(canvas);
        drawContent(canvas);
    }

    private void drawContent(Canvas canvas) {


    }

    private void drawCenter(Canvas canvas) {
        canvas.drawCircle(mCenterX, mCenterY, mMinRadius, mPaint);
    }

    private void drawBackground(Canvas canvas) {
        canvas.drawCircle(mCenterX, mCenterY, getMeasuredWidth() / 2 - 10, mBackgroundPaint);
    }
}
