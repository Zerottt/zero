package zero.vga;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Scroller;

/**
 * Created by tengtao on 16/9/8.
 */
public class ScrollerView extends ViewGroup {
    private int mScreenHeight;
    private int mLastY;
    private int mStart;
    private Scroller mScroller;
    private int mEnd;

    public ScrollerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        mScroller = new Scroller(context);

    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View view = getChildAt(i);
            measureChild(view, widthMeasureSpec, heightMeasureSpec);
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int childCount = getChildCount();
        MarginLayoutParams params = (MarginLayoutParams) getLayoutParams();
        params.height = childCount * mScreenHeight;
        setLayoutParams(params);

        for (int i = 0; i < childCount; i++) {
            View childView = getChildAt(i);
            if (childView.getVisibility() != View.GONE) {
                childView.layout(l, mScreenHeight * i, r, mScreenHeight * (i + 1));
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int y = (int) event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mLastY = y;
                mStart = getScrollY();
                break;
            case MotionEvent.ACTION_MOVE:
                if (!mScroller.isFinished()) {
                    mScroller.abortAnimation();
                }

                int dy = mLastY - y;

//                if (getScrollY() < 0) {
//                    dy = 0;
//                }
//                if (getScrollY() > getHeight() - mScreenHeight) {
//                    dy = 0;
//                }
                scrollBy(0, dy);
                mLastY = y;
                break;
            case MotionEvent.ACTION_UP:
                mEnd = getScrollY();
                int dScrollY = mEnd - mStart;
                if (dScrollY > 0) {
                    if (dScrollY < mScreenHeight / 3) {
                        mScroller.startScroll(0, getScrollY(), 0, -dScrollY);
                    } else {
                        mScroller.startScroll(0, getScrollY(), 0, mScreenHeight - dScrollY);
                    }
                } else {
                    if (-dScrollY < mScreenHeight / 3) {
                        mScroller.startScroll(0, getScrollY(), 0, -dScrollY);
                    } else {
                        mScroller.startScroll(0, getScrollY(), 0, -mScreenHeight - dScrollY);
                    }
                }
                break;
        }

        ((ViewGroup)getParent()).requestDisallowInterceptTouchEvent(false);
        postInvalidate();

        return true;
    }

    @Override
    public void computeScroll() {
        super.computeScroll();
        //判断mScroller是否执行完毕
        if (mScroller.computeScrollOffset()) {
            scrollTo(0, mScroller.getCurrY());
            //通过重绘不断的调用computeScrol
            postInvalidate();
        }

    }

}

