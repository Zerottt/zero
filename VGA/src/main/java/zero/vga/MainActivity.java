package zero.vga;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.drawerLayout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.linearLayout)
    LinearLayout mLinearLayout;
    @BindView(R.id.txt_countDown)
    TextView mTxtCountDown;
    //    @BindView(R.id.listview)
//     OverScrollListView mListview;
    private ActionBarDrawerToggle mDrawerToggle;


    private List<String> mStrings = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//        StatusBarUtil.setColor(this, getResources().getColor(R.color.red));
        StatusBarUtil.setColorForDrawerLayout(this, mDrawerLayout, getResources().getColor(R.color.colorAccent));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, 0, 0) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                mAnimationDrawable.stop();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
//                mAnimationDrawable.start();
            }
        };
        mDrawerToggle.syncState();
        mDrawerLayout.setDrawerListener(mDrawerToggle);


//        mListview.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,mStrings));


    }


    public void addViewClick(View view) {
//        TextView textView = new TextView(this);
//        textView.setText("dddddddd");
//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 100);
//        textView.setBackgroundColor(Color.RED);
//        textView.setGravity(Gravity.CENTER);
//        params.topMargin = 20;
//        mLinearLayout.addView(textView, params);


//        ScaleAnimation scaleAnimation=new ScaleAnimation(0,1,0,1);
//        CustomAnimation scaleAnimation=new CustomAnimation();
//        scaleAnimation.setDuration(500);
//        textView.startAnimation(scaleAnimation);

//        LayoutAnimationController controller=new LayoutAnimationController(scaleAnimation,0);
//        controller.setOrder(LayoutAnimationController.ORDER_REVERSE);
//        mLinearLayout.setLayoutAnimation(controller);

        ValueAnimator valueAnimator = ValueAnimator.ofInt(30, 0);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int countDown = (Integer) animation.getAnimatedValue();
                mTxtCountDown.setText(countDown + "");
            }
        });

        valueAnimator.setDuration(30 * 1000);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.start();


    }
}
