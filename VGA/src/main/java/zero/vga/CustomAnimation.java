package zero.vga;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.Transformation;

/**
 * Created by tengtao on 16/9/10.
 */
public class CustomAnimation extends Animation {

    private int mCenterX;
    private int mCenterY;
    private Camera mCamera;
    private float mRotateY=10f;


    public CustomAnimation() {
        mCamera = new Camera();
    }

    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
        setDuration(2000);
        setFillAfter(true);
        setInterpolator(new BounceInterpolator());
        mCenterX = width / 2;
        mCenterY = height / 2;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        Matrix matrix = t.getMatrix();
        mCamera.save();
        mCamera.rotateY(100 * interpolatedTime);
        mCamera.getMatrix(matrix);
        matrix.preTranslate(-mCenterX, -mCenterY);
        matrix.postTranslate(0, 0);
        mCamera.restore();


//        Matrix matrix = t.getMatrix();
//        mCamera.save();
//        mCamera.translate(0f, 0f, (1300 - 1300*interpolatedTime));
//        mCamera.rotateY(360*interpolatedTime);
//        mCamera.getMatrix(matrix);
//        matrix.preTranslate(-mCenterX, -mCenterY);
//        matrix.postTranslate(mCenterX,mCenterY);
//        mCamera.restore();
    }
}
