package zero.myapplication;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tengtao on 2016/10/9.
 */

public class RefreshActivity extends AppCompatActivity {
    private LoadMoreRecyclerView mRecyclerView;
    private Toolbar mToolbar;

    private LoadMoreHeaderAdapter mLoadMoreHeaderAdapter;
    private int index;

    private View mHeaderView;
    private View mFooterView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refresh);
        initToolbar();
        mRecyclerView = (LoadMoreRecyclerView) findViewById(R.id.id_recycler_view);
//        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
//        mRecyclerView.setLayoutManager(new GridLayoutManager(this,3));


        mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        mHeaderView = createView(Color.RED, "Header view");
        mFooterView = createView(Color.GREEN, "Footer view");

        TestBaseAdapter mAdapter = new TestBaseAdapter(this);
        mLoadMoreHeaderAdapter = new LoadMoreHeaderAdapter(mAdapter);
//        mLoadMoreHeaderAdapter.setHeaderView(mHeaderView);
//        mLoadMoreHeaderAdapter.setFooterView(mFooterView);
//        mLoadMoreHeaderAdapter.setOpenLoadMore(true);


        mRecyclerView.setAdapter(mLoadMoreHeaderAdapter);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mLoadMoreHeaderAdapter.refresh(setUpDatas(true));
            }
        }, 2000);

        mRecyclerView.setOnLoadMoreListener(new LoadMoreRecyclerView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mLoadMoreHeaderAdapter.loadMore(setUpDatas(false));
                    }
                }, 2000);
            }
        });


    }

    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    private TextView createView(int color, String text) {
        TextView textView = new TextView(this);
        textView.setText(text);
        textView.setBackgroundColor(color);
        textView.setHeight(200);
        textView.setGravity(Gravity.CENTER);
        textView.setWidth(getResources().getDisplayMetrics().widthPixels);
        return textView;
    }

    public void emptyClick(View view) {

    }

    public void errorClick(View view) {

    }

    public void reloadClick(View view) {
    }

    private List<String> setUpDatas(boolean isRefresh) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add("datas -->" + index);
            index++;
        }
        return list;
    }


    class TestBaseAdapter extends BaseAdapter<String> {


        public TestBaseAdapter(Context context) {
            super(context);
        }

        @Override
        protected int getItemLayoutId() {
            return android.R.layout.simple_list_item_1;
        }

        @Override
        protected void convert(ViewHolder viewHolder, String item) {
            viewHolder.setText(android.R.id.text1, item);
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_recycler, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.showEmpty:
                mRecyclerView.setViewState(LoadMoreRecyclerView.STATE_EMPTY);
                mLoadMoreHeaderAdapter.refresh(null);
                break;
            case R.id.showError:
                mRecyclerView.setViewState(LoadMoreRecyclerView.STATE_ERROR);
                mLoadMoreHeaderAdapter.refresh(null);
                break;
            case R.id.refresh:
                index = 0;
                mLoadMoreHeaderAdapter.refresh(setUpDatas(true));
                break;
            case R.id.loadMore:
                mLoadMoreHeaderAdapter.loadMore(setUpDatas(true));
                break;
            case R.id.showHeader:
                mLoadMoreHeaderAdapter.setHeaderView(mHeaderView);
                mLoadMoreHeaderAdapter.notifyDataSetChanged();
                break;
            case R.id.showFooter:
                mLoadMoreHeaderAdapter.setFooterView(mFooterView);
                mLoadMoreHeaderAdapter.setOpenLoadMore(true);
                mLoadMoreHeaderAdapter.notifyDataSetChanged();
                break;
            case R.id.linearLayout:
                mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                break;
            case R.id.gridLayout:
                mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
                break;
            case R.id.staggered:
                mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

                break;
        }

        return super.onOptionsItemSelected(item);
    }

}
