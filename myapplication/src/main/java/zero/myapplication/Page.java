package zero.myapplication;

/**
 * Created by tengtao on 2016/10/9.
 */

public class Page {
    private int mCurrentPage = 1;
    private int mTotalePage;


    public int getCurrentPage() {
        return mCurrentPage;
    }

    public void setCurrentPage(int currentPage) {
        mCurrentPage = currentPage;
    }

    public int getTotalePage() {
        return mTotalePage;
    }

    public void setTotalePage(int totalePage) {
        mTotalePage = totalePage;
    }

    public boolean isMore() {
        return mCurrentPage < mTotalePage;
    }

    public void refresh(int totalePage) {
        mCurrentPage = 2;
        mTotalePage = totalePage;
    }
}
