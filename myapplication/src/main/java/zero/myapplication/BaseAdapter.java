package zero.myapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tengtao on 2016/10/9.
 */

public abstract class BaseAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<T> mDatas;

    private OnItemClickListener<T> mOnItemClickListener;


    protected abstract int getItemLayoutId();

    protected abstract void convert(ViewHolder viewHolder, T item);


    public BaseAdapter(Context context) {
        this(context, null);
    }

    public BaseAdapter(Context context, List<T> datas) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mDatas = datas == null ? new ArrayList<T>() : datas;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ViewHolder.create(mLayoutInflater, getItemLayoutId());
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        bindItemView(viewHolder, position);
    }

    void bindItemView(final ViewHolder viewHolder, final int position) {
        convert(viewHolder, getItem(position));
        if (mOnItemClickListener == null) return;
        viewHolder.getItemView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(viewHolder, getItem(position), position);
            }
        });
    }


    public T getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public void refresh(List<T> datas) {

        if (datas==null){
            datas=new ArrayList<>();
        }
        mDatas.clear();
        mDatas.addAll(datas);
        notifyDataSetChanged();
    }

    public void loadMore(List<T> datas) {
        if (isEmpty(datas)) return;
        int positionStart = getItemCount();
        mDatas.addAll(datas);
        notifyItemInserted(positionStart);
    }


    public boolean isEmpty(List<T> datas) {
        if (datas == null || datas.size() == 0) {
            return true;
        }
        return false;
    }

    public interface OnItemClickListener<T> {
        void onItemClick(ViewHolder viewHolder, T item, int position);
    }
}
