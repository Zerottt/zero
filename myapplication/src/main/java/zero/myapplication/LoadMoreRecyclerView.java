package zero.myapplication;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewStub;
import android.widget.FrameLayout;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static zero.myapplication.MultiStateView.VIEW_STATE_CONTENT;
import static zero.myapplication.MultiStateView.VIEW_STATE_EMPTY;
import static zero.myapplication.MultiStateView.VIEW_STATE_ERROR;
import static zero.myapplication.MultiStateView.VIEW_STATE_LOADING;

/**
 * Created by tengtao on 2016/10/10.
 */

public class LoadMoreRecyclerView extends FrameLayout {


    public static final int STATE_ERROR = 1;

    public static final int STATE_EMPTY = 2;


    @Retention(RetentionPolicy.SOURCE)
    @IntDef({STATE_ERROR, STATE_EMPTY})
    public @interface State {
    }


    private int mSuperRecyclerViewMainLayout;
    private int mEmptyId;
    private int mMoreProgressId;
    private int mProgressId;
    private int mErroerId;
    protected boolean mClipToPadding;
    protected int mPadding;
    protected int mPaddingTop;
    protected int mPaddingBottom;
    protected int mPaddingLeft;
    protected int mPaddingRight;
    protected int mScrollbarStyle;
    protected boolean isMoreEnable;
    protected int mMoreProgressHeight;

    private MultiStateView mMultiStateView;


    //    private ViewStub mProgress;
//    private View mProgressView;
    private ViewStub mMoreProgress;
    private View mMoreProgressView;
    //    private ViewStub mEmpty;
//    private View mEmptyView;
    private RecyclerView mRecycler;
    private boolean isLoadingMore;

    protected Context mContext;

    protected LAYOUT_MANAGER_TYPE layoutManagerType;
    private int[] lastScrollPositions;


    private OnLoadMoreListener mOnLoadMoreListener;
    @State
    private int mViewState = VIEW_STATE_EMPTY;


    public LoadMoreRecyclerView(Context context) {
        this(context, null);
    }

    public LoadMoreRecyclerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LoadMoreRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        initAttrs(attrs);
        initViews();
    }


    private void initAttrs(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.LoadMoreRecyclerView);
        try {
            mSuperRecyclerViewMainLayout = a.getResourceId(R.styleable.LoadMoreRecyclerView_mainLayoutId, R.layout.layout_progress_recyclerview);
            mClipToPadding = a.getBoolean(R.styleable.LoadMoreRecyclerView_recyclerClipToPadding, false);
            mPadding = (int) a.getDimension(R.styleable.LoadMoreRecyclerView_recyclerPadding, -1.0f);
            mPaddingTop = (int) a.getDimension(R.styleable.LoadMoreRecyclerView_recyclerPaddingTop, 0.0f);
            mPaddingBottom = (int) a.getDimension(R.styleable.LoadMoreRecyclerView_recyclerPaddingBottom, 0.0f);
            mPaddingLeft = (int) a.getDimension(R.styleable.LoadMoreRecyclerView_recyclerPaddingLeft, 0.0f);
            mPaddingRight = (int) a.getDimension(R.styleable.LoadMoreRecyclerView_recyclerPaddingRight, 0.0f);
            mScrollbarStyle = a.getInt(R.styleable.LoadMoreRecyclerView_scrollbarStyle, -1);
            mEmptyId = a.getResourceId(R.styleable.LoadMoreRecyclerView_layout_empty, -1);
            mMoreProgressId = a.getResourceId(R.styleable.LoadMoreRecyclerView_layout_moreProgress, R.layout.layout_more_progress);
            mMoreProgressHeight = a.getDimensionPixelSize(R.styleable.LoadMoreRecyclerView_moreProgressHeight, -1);
            mProgressId = a.getResourceId(R.styleable.LoadMoreRecyclerView_layout_progress, R.layout.layout_progress);
            mErroerId = a.getResourceId(R.styleable.LoadMoreRecyclerView_layout_error, -1);
            isMoreEnable = a.getBoolean(R.styleable.LoadMoreRecyclerView_moreEnable, true);
        } finally {
            a.recycle();
        }
    }

    private void initViews() {

        if (isInEditMode()) {
            return;
        }

        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(mSuperRecyclerViewMainLayout, this);

        mMultiStateView = (MultiStateView) view.findViewById(R.id.multiStateView);

        mMoreProgress = (ViewStub) view.findViewById(R.id.more_progress);
        mMoreProgress.setLayoutResource(mMoreProgressId);
        if (mMoreProgressId != 0) {
            mMoreProgressView = mMoreProgress.inflate();
            if (mMoreProgressHeight > 0) {
                mMoreProgressView.getLayoutParams().height = mMoreProgressHeight;
            }
        }
        mMoreProgress.setVisibility(GONE);

//        mProgress = (ViewStub) view.findViewById(android.R.id.progress);
//        mProgress.setLayoutResource(mProgressId);
//        mProgressView = mProgress.inflate();

//        mEmpty = (ViewStub) view.findViewById(R.id.empty);
//        mEmpty.setLayoutResource(mEmptyId);
//        if (mEmptyId != 0) {
//            mEmptyView = mEmpty.inflate();
//            mEmptyView.setOnClickListener(mEmptyClickListener);
//        }
//        mEmpty.setVisibility(GONE);

        mMultiStateView.addMaskView(VIEW_STATE_LOADING, mProgressId);
        mMultiStateView.addMaskView(VIEW_STATE_EMPTY, mEmptyId);
        mMultiStateView.addMaskView(VIEW_STATE_ERROR, mErroerId);


        initRecyclerView(view);
    }

    public void setViewState(@State int state) {
        mViewState = state;
    }

    public void setLayoutManager(RecyclerView.LayoutManager layout) {
        layoutManagerType = null;
        mRecycler.setLayoutManager(layout);
        if (layout instanceof GridLayoutManager) {
            RecyclerView.Adapter adapter = mRecycler.getAdapter();
            if (adapter != null) {
                adapter.onAttachedToRecyclerView(mRecycler);
            }
        }
    }

    public void setAdapter(final LoadMoreHeaderAdapter adapter) {
        mRecycler.setAdapter(adapter);

//        mProgress.setVisibility(View.GONE);
//        mRecycler.setVisibility(View.VISIBLE);


        if (null != adapter) {
            adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                @Override
                public void onItemRangeChanged(int positionStart, int itemCount) {
                    super.onItemRangeChanged(positionStart, itemCount);
                    update();
                }

                @Override
                public void onItemRangeInserted(int positionStart, int itemCount) {
                    super.onItemRangeInserted(positionStart, itemCount);
                    update();
                }

                @Override
                public void onItemRangeRemoved(int positionStart, int itemCount) {
                    super.onItemRangeRemoved(positionStart, itemCount);
                    update();
                }

                @Override
                public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
                    super.onItemRangeMoved(fromPosition, toPosition, itemCount);
                    update();
                }

                @Override
                public void onChanged() {
                    super.onChanged();
                    update();
                }

                private void update() {

//                    mProgress.setVisibility(View.GONE);

                    mMoreProgress.setVisibility(View.GONE);
                    isLoadingMore = false;
                    int headerFooterCount = adapter.getFooterCount() + adapter.getHeaderCount();
                    if (adapter.getItemCount() - headerFooterCount == 0) {
                        mMultiStateView.setViewState(mViewState);
                        return;
                    }

                    if (adapter.getItemCount() - headerFooterCount > 0) {
                        mRecycler.setVisibility(View.VISIBLE);
                        mMultiStateView.setViewState(VIEW_STATE_CONTENT);
                    }

//                    if (adapter1.getItemCount() > 0) {
//                        mPtrLayout.setVisibility(View.VISIBLE);
//                    } else {
//                        mPtrLayout.setVisibility(View.GONE);
//                    }

                }
            });
        }


    }

    public RecyclerView getRecyclerView() {
        return mRecycler;
    }

    public View getLoadingView() {
        return mMultiStateView.getLoadingView();
    }

    public View getErrorView() {
        return mMultiStateView.getErrorView();
    }

    public View getEmptyView() {
        return mMultiStateView.getEmptyView();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        mOnLoadMoreListener = onLoadMoreListener;
    }


    private void initRecyclerView(View view) {
        View v = view.findViewById(android.R.id.list);
        if (v instanceof RecyclerView) {
            mRecycler = (RecyclerView) v;
        } else {
            throw new IllegalArgumentException("SuperRecyclerView works with a RecyclerView!");
        }


        mRecycler.setClipToPadding(mClipToPadding);
        mRecycler.addOnScrollListener(mInternalOnScrollListener);
        if (!FloatUtil.compareFloats(mPadding, -1.0f)) {
            mRecycler.setPadding(mPadding, mPadding, mPadding, mPadding);
        } else {
            mRecycler.setPadding(mPaddingLeft, mPaddingTop, mPaddingRight, mPaddingBottom);
        }

        if (mScrollbarStyle != -1) {
            mRecycler.setScrollBarStyle(mScrollbarStyle);
        }

    }

    private OnClickListener mEmptyClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    private RecyclerView.OnScrollListener mInternalOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            processOnMore(dy);
        }


    };

    private void processOnMore(int dy) {
        RecyclerView.LayoutManager layoutManager = mRecycler.getLayoutManager();
        int lastVisibleItemPosition = getLastVisibleItemPosition(layoutManager);
        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();

        if (!isLoadingMore && dy > 0 && lastVisibleItemPosition >= totalItemCount - 1) {
            //此时是刷新状态
            if (mOnLoadMoreListener != null) {
                mMoreProgress.setVisibility(View.VISIBLE);
                mOnLoadMoreListener.onLoadMore();
            }
            isLoadingMore = true;
        }
    }

    private int getLastVisibleItemPosition(RecyclerView.LayoutManager layoutManager) {
        int lastVisibleItemPosition = -1;
        if (layoutManagerType == null) {
            if (layoutManager instanceof GridLayoutManager) {
                layoutManagerType = LAYOUT_MANAGER_TYPE.GRID;
            } else if (layoutManager instanceof LinearLayoutManager) {
                layoutManagerType = LAYOUT_MANAGER_TYPE.LINEAR;
            } else if (layoutManager instanceof StaggeredGridLayoutManager) {
                layoutManagerType = LAYOUT_MANAGER_TYPE.STAGGERED_GRID;
            } else {
                throw new RuntimeException("Unsupported LayoutManager used. Valid ones are LinearLayoutManager, GridLayoutManager and StaggeredGridLayoutManager");
            }
        }

        switch (layoutManagerType) {
            case LINEAR:
                lastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
                break;
            case GRID:
                lastVisibleItemPosition = ((GridLayoutManager) layoutManager).findLastVisibleItemPosition();
                break;
            case STAGGERED_GRID:
                lastVisibleItemPosition = caseStaggeredGrid(layoutManager);
                break;
        }
        return lastVisibleItemPosition;
    }

    private int caseStaggeredGrid(RecyclerView.LayoutManager layoutManager) {

        StaggeredGridLayoutManager staggeredGridLayoutManager = (StaggeredGridLayoutManager) layoutManager;
        if (lastScrollPositions == null) {
            lastScrollPositions = new int[staggeredGridLayoutManager.getSpanCount()];
        }
        staggeredGridLayoutManager.findLastVisibleItemPositions(lastScrollPositions);
        return findMax(lastScrollPositions);
    }

    private int findMax(int[] lastPositions) {
        int max = Integer.MIN_VALUE;
        for (int value : lastPositions) {
            if (value > max)
                max = value;
        }
        return max;
    }


    public enum LAYOUT_MANAGER_TYPE {
        LINEAR,
        GRID,
        STAGGERED_GRID
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public static class FloatUtil {
        private final static float EPSILON = 0.00000001F;

        public static boolean compareFloats(float f1, float f2) {
            return Math.abs(f1 - f2) <= EPSILON;
        }
    }

}
