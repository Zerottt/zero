package zero.myapplication;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

/**
 * Created by tengtao on 2016/10/9.
 */

public class ViewHolder extends RecyclerView.ViewHolder {
    private SparseArray<View> mViews;
    private View mItemView;

    private ViewHolder(View itemView) {
        super(itemView);
        mItemView = itemView;
        mViews = new SparseArray<>();
    }


    public static ViewHolder create(Context context, @LayoutRes int layoutId) {
        return create(LayoutInflater.from(context), layoutId);
    }

    public static ViewHolder create(@NonNull LayoutInflater inflater, @LayoutRes int layoutId) {
        View view = inflater.inflate(layoutId, null);
        return create(view);
    }

    public static ViewHolder create(@NonNull View itemView) {
        return new ViewHolder(itemView);
    }

    public View getItemView() {
        return mItemView;
    }

    public <T extends View> T getView(@IdRes int viewId) {
        View view = mViews.get(viewId);
        if (view == null) {
            view = mItemView.findViewById(viewId);
            mViews.put(viewId, view);
        }
        return (T) view;
    }


    public void setText(@IdRes int viewId, String text) {
        ((TextView) getView(viewId)).setText(TextUtils.isEmpty(text) ? "" : text);
    }

}
