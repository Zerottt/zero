package zero.myapplication;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tengtao on 2016/10/10.
 */

public class LoadMoreHeaderAdapter extends RecyclerView.Adapter {

    private static final int TYPE_HEADER = 0X001;
    private static final int TYPE_FOOTTER = 0X002;

    private BaseAdapter mBaseAdapter;

    private List<View> mHeaderViews = new ArrayList<>();
    private List<View> mFooterViews = new ArrayList<>();
    private boolean mOpenLoadMore;


    public LoadMoreHeaderAdapter(BaseAdapter baseAdapter) {
        mBaseAdapter = baseAdapter;
    }


    public void setHeaderView(View headerView) {
        mHeaderViews.clear();
        mHeaderViews.add(headerView);
    }

    public void setFooterView(View footerView) {
        mFooterViews.clear();
        mFooterViews.add(footerView);
    }

    public void setOpenLoadMore(boolean openLoadMore) {
        mOpenLoadMore = openLoadMore;
    }

    @Override
    public int getItemViewType(int position) {
        if (isHeaderView(position)) {
            return TYPE_HEADER;
        }
        if (isFooterView(position)) {
            return TYPE_FOOTTER;
        }
        return mBaseAdapter.getItemViewType(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            return ViewHolder.create(mHeaderViews.get(0));
        } else if (viewType == TYPE_FOOTTER) {
            return ViewHolder.create(mFooterViews.get(0));
        } else {
            return mBaseAdapter.onCreateViewHolder(parent, viewType);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        if (viewType == TYPE_HEADER) {
        } else if (viewType == TYPE_FOOTTER) {
        } else {
            mBaseAdapter.onBindViewHolder(holder, position - mHeaderViews.size());
        }
    }

    @Override
    public int getItemCount() {
        return mBaseAdapter.getItemCount() + mHeaderViews.size() + getFooterSize();
    }

    private int getFooterSize() {
        return mOpenLoadMore ? mFooterViews.size() : 0;
    }

    private boolean isHeaderView(int position) {
        return mHeaderViews.size() > 0 && position == 0;
    }

    private boolean isFooterView(int position) {
        return mFooterViews.size() > 0 && mOpenLoadMore && position >= getItemCount() - 1;
    }


    public <T> void refresh(List<T> datas) {
        mBaseAdapter.refresh(datas);
        notifyDataSetChanged();
    }


    public <T> void loadMore(List<T> datas) {
        int positionStart = getItemCount();
        mBaseAdapter.loadMore(datas);
        notifyItemInserted(positionStart);
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        final RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager) {
            ((GridLayoutManager) layoutManager).setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (isHeaderView(position) || isFooterView(position)) {
                        return ((GridLayoutManager) layoutManager).getSpanCount();
                    }
                    return 1;
                }
            });
        }
    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);

        int position = holder.getLayoutPosition();
        if (isFooterView(position) || isHeaderView(position)) {
            ViewGroup.LayoutParams params = holder.itemView.getLayoutParams();
            if (params != null && params instanceof StaggeredGridLayoutManager.LayoutParams) {
                ((StaggeredGridLayoutManager.LayoutParams) params).setFullSpan(true);
            }
        }

    }

    public BaseAdapter getBaseAdapter() {
        return mBaseAdapter;
    }

    public int getHeaderCount() {
        return mHeaderViews.size();
    }

    public int getFooterCount() {
        return mFooterViews.size();
    }
}
