package com.design.originator;

/**
 * Created by tengtao on 2016/9/27.
 */

public class Client {
    public static void main(String[] args) {
        CallOfDuty game = new CallOfDuty();
        game.play();


        Caretaker caretaker = new Caretaker();
        caretaker.archive(game.createMomoto());

        game.quit();
        CallOfDuty newGame = new CallOfDuty();
        newGame.restore(caretaker.getMomoto());
    }
}
