package com.design.originator;

/**
 * Created by tengtao on 2016/9/27.
 * 管理备忘录
 */

public class Caretaker {
    Momoto mMomoto;

    /**
     * 存档
     * @param momoto
     */
    public void archive(Momoto momoto){
        this.mMomoto=momoto;

    }

    /**
     * 获取存档
     * @return
     */
    public Momoto getMomoto() {
        return mMomoto;
    }
}
