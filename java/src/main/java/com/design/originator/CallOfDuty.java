package com.design.originator;

/**
 * Created by tengtao on 2016/9/27.
 */

public class CallOfDuty {
    private int mCheckPoint = 1;
    private int mLifeValue = 100;
    private String mWeapon = "沙漠之鹰";


    public void play() {
        System.out.println("玩游戏:" + String.format("第%d关", mCheckPoint) + " 奋斗杀敌中");
        mLifeValue -= 10;
        System.out.println("进度升级了");
        mCheckPoint++;
        System.out.println("到达 " + String.format("第%d关", mCheckPoint));
    }

    public void quit() {
        System.out.println("--------------------");
        System.out.println("退出前游戏属性:" + this.toString());
        System.out.println("退出游戏");
        System.out.println("--------------------");
    }

    public Momoto createMomoto() {
        Momoto momoto = new Momoto();
        momoto.mCheckPoint = mCheckPoint;
        momoto.mLifeValue = mLifeValue;
        momoto.mWeapon = mWeapon;
        return momoto;
    }

    public void restore(Momoto momoto) {
        this.mCheckPoint = momoto.mCheckPoint;
        this.mLifeValue = momoto.mLifeValue;
        this.mWeapon = momoto.mWeapon;

        System.out.println("恢复后属性为:"+this.toString());

    }

    @Override
    public String toString() {
        return "CallOfDuty{" +
                "mCheckPoint=" + mCheckPoint +
                ", mLifeValue=" + mLifeValue +
                ", mWeapon='" + mWeapon + '\'' +
                '}';
    }
}
