package com.design.originator;

/**
 * Created by tengtao on 2016/9/27.
 * 备忘录类
 */
public class Momoto {
    public int mCheckPoint ;
    public int mLifeValue;
    public String mWeapon;

    @Override
    public String toString() {
        return "Momoto{" +
                "mCheckPoint=" + mCheckPoint +
                ", mLifeValue=" + mLifeValue +
                ", mWeapon='" + mWeapon + '\'' +
                '}';
    }
}
