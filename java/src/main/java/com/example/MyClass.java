package com.example;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyClass {
    public static void main(String[] args) {
//        int[] arr = new int[10];
//
//        arr[0] = 1;
//        arr[1] = 2;
//        arr[2] = 3;
//        arr[3] = 4;
//        arr[4] = 5;
//        arr[5] = 6;
//
//        print(arr);
//
//        insert(arr, 3, 10);
//
//        System.out.println("--------");
//        print(arr);
//
//        delete(arr, 3);
//
//        System.out.println("----------------");
//        print(arr);

//        int[] arr=new int[40];
//        arr[0]=0;
//        arr[1]=1;
//
//        for (int i = 2; i < 13; i++) {
//            arr[i]=arr[i-1]+arr[i-2];
//            System.out.println("arr["+i+"]="+arr[i]);
//        }


//        System.out.println("STRING".equalsIgnoreCase("string"));



        String str = "123abc你好efc";
        String reg = "[\u4e00-\u9fa5]";
        Pattern pat = Pattern.compile(reg);
        Matcher mat=pat.matcher(str);
        MatchResult matchResult = mat.toMatchResult();

        System.out.println(matchResult.start());

        String repickStr = mat.replaceAll("");
        System.out.println("去中文后:"+repickStr);

    }

    private static void delete(int[] arr, int index) {
        int length = arr.length;
        if (index > length - 1 || index < 0) {
            System.out.println("outoffarraysindex");
            return;
        }

        for (int i = index; i < arr.length; i++) {
            arr[i - 1] = arr[i];
        }
    }

    private static void insert(int[] arr, int index, int e) {
        int length = arr.length;
        if (index > length - 1 || index < 0) {
            System.out.println("outoffarraysindex");
            return;
        }

        for (int i = arr.length - 1; i >= index; i--) {

            arr[i] = arr[i - 1];
        }

        arr[index - 1] = e;

    }

    private static void print(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println("arr[" + i + "]=" + arr[i]);
        }
    }
}
