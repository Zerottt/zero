package zero.dagger;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tengtao on 2016/9/26.
 */

@Module
public class ActivityModle {
    @Provides
    @ActivityLift
    public UserModle provideUserModle() {
        return new UserModle();
    }
}
