package zero.dagger;

import dagger.Component;

/**
 * Created by tengtao on 2016/9/26.
 */
@ActivityLift
@Component(modules = ActivityModle.class)
public interface ActivityComponent {
    void inject(MainActivity activity);



}
