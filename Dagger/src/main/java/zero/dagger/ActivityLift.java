package zero.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by tengtao on 2016/9/26.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityLift {
}
