package zero.dagger;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    private TextView mTextView;

    @Inject
    UserModle mUserModle;

    @Inject
    UserModle mUserModle1;

    ActivityComponent mComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextView = (TextView) findViewById(R.id.txt_result);

        mComponent = DaggerActivityComponent.builder()
                .activityModle(new ActivityModle()).build();
        mComponent.inject(this);

    }

    public void onDaggerClick(View view) {
        Log.e("debug","mUserModl="+mUserModle.hashCode());
        Log.e("debug","mUserModl1="+mUserModle1.hashCode());

        mUserModle.setId((int) (Math.random()*100));
        mUserModle.setName("jim");
        mUserModle.setAge(32);
        mTextView.setText("id=" + mUserModle.getId() + "  name=" + mUserModle.getName() + "  age=" + mUserModle.getAge());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
