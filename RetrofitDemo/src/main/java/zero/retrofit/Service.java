package zero.retrofit;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import zero.retrofit.bean.BaseBean;
import zero.retrofit.bean.UserInfo;

/**
 * Created by tengtao on 16/8/18.
 */
public interface Service {
    @FormUrlEncoded
    @POST("member/login")
    Call<BaseBean<UserInfo>> login(@Field("cMob") String mobile, @Field("cPass") String password
            , @Field("deviceType") String deviceType, @Field("deviceId") String deviceId);

    @POST("member/login")
    Call<BaseBean<UserInfo>> login(@Body LoginRequestParams params);

    @FormUrlEncoded
    @POST("member/login")
    Call<BaseBean<UserInfo>> login(@FieldMap Map<String, String> params);

}
