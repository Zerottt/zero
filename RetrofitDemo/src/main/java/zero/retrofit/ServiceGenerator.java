package zero.retrofit;

import android.util.Log;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by tengtao on 16/8/18.
 */
public class ServiceGenerator {
    public final static String PATH = "http://139.196.190.206:8088/";
    public static final String BASE_URL = PATH + "webmvc/api/"; // 总路径

    private Retrofit mServiceGenerator;

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    public static <T> T createService(Class<T> cls) {

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();

                RequestBody oidBody = originalRequest.body();


                Map<String, String> params = new HashMap<>();


                if (oidBody instanceof FormBody) {
                    FormBody.Builder build = new FormBody.Builder();
                    FormBody formBody = (FormBody) oidBody;
                    for (int i = 0; i < formBody.size(); i++) {
                        build.add(formBody.encodedName(i), formBody.encodedValue(i));
                    }

                    build.add("version", "65");
                    oidBody = build.build();
                    FormBody formBody1 = (FormBody) oidBody;
                    for (int i = 0; i < formBody1.size(); i++) {
                        params.put(formBody1.encodedName(i), formBody1.encodedValue(i));
                    }
                }
                Log.e("debug", "params=" + params.toString());


                return chain.proceed(originalRequest);
            }
        });
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(cls);
    }


}
