package zero.retrofit;

/**
 * Created by tengtao on 16/8/18.
 */
public class ServiceClient {
    private static Service service;

    public static Service getService() {
        if (service == null) {
            service = ServiceGenerator.createService(Service.class);
        }
        return service;
    }
}
