package zero.retrofit;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zero.retrofit.bean.BaseBean;
import zero.retrofit.bean.UserInfo;

/**
 * Created by tengtao on 16/8/18.
 */
public class LoginHelper {
    public static void login() {
        Service service = ServiceClient.getService();


        Call<BaseBean<UserInfo>> call = service.login("18672791326", "123456", "1", "123");
        call.enqueue(new Callback<BaseBean<UserInfo>>() {

            @Override
            public void onResponse(Call<BaseBean<UserInfo>> call, Response<BaseBean<UserInfo>> response) {
                Request request = call.request();
                if (response.isSuccessful()) {
                    BaseBean<UserInfo> baseBean = response.body();
                    Log.e("debug", "baseBean=" + baseBean.toString());
                    UserInfo datas = baseBean.getDatas();

                } else {
                    onFailure(call, null);
                }
//                Log.e("debug", "msg=" + response.message());
//                Log.e("debug", "code=" + response.code());
//                Log.e("debug", "isSuccessful=" + response.isSuccessful());
//                Log.e("debug","errorBody"+response.errorBody().toString());
//                Log.e("debug", "content=" + response.body().toString());
            }

            @Override
            public void onFailure(Call<BaseBean<UserInfo>> call, Throwable t) {
                if (t != null)
                    Log.e("debug", "errror=" + t.toString());
                else {
                    Log.e("debug", "errror=");
                }
            }
        });
    }

    public static void login2() {
        Service service = ServiceClient.getService();

        Map<String,String> map=new HashMap<>();
        map.put("cMob","18672791326");
        map.put("cPass","123456");
        map.put("deviceType","18672791326");
        map.put("deviceId","123");

        Call<BaseBean<UserInfo>> call = service.login(map);
        call.enqueue(new Callback<BaseBean<UserInfo>>() {

            @Override
            public void onResponse(Call<BaseBean<UserInfo>> call, Response<BaseBean<UserInfo>> response) {
                Request request = call.request();
                if (response.isSuccessful()) {
                    BaseBean<UserInfo> baseBean = response.body();

                    Log.e("debug", "err=" + baseBean.getError());
                    Log.e("debug", "baseBean=" + baseBean.toString());
                    UserInfo datas = baseBean.getDatas();

                } else {
                    onFailure(call, null);
                }
//                Log.e("debug", "msg=" + response.message());
//                Log.e("debug", "code=" + response.code());
//                Log.e("debug", "isSuccessful=" + response.isSuccessful());
//                Log.e("debug","errorBody"+response.errorBody().toString());
//                Log.e("debug", "content=" + response.body().toString());
            }

            @Override
            public void onFailure(Call<BaseBean<UserInfo>> call, Throwable t) {
                if (t != null)
                    Log.e("debug", "errror=" + t.toString());
                else {
                    Log.e("debug", "errror=");
                }
            }
        });
    }

    public static void login1() {
        Service service = ServiceClient.getService();

        LoginRequestParams params = new LoginRequestParams("18672791326", "123456");
        Call<BaseBean<UserInfo>> call = service.login(params);
        call.enqueue(new Callback<BaseBean<UserInfo>>() {

            @Override
            public void onResponse(Call<BaseBean<UserInfo>> call, Response<BaseBean<UserInfo>> response) {
                Request request = call.request();
                if (response.isSuccessful()) {
                    BaseBean<UserInfo> baseBean = response.body();
                    Log.e("debug", "baseBean=" + baseBean.toString());
                    UserInfo datas = baseBean.getDatas();

                } else {
                    onFailure(call, null);
                }
//                Log.e("debug", "msg=" + response.message());
//                Log.e("debug", "code=" + response.code());
//                Log.e("debug", "isSuccessful=" + response.isSuccessful());
//                Log.e("debug","errorBody"+response.errorBody().toString());
//                Log.e("debug", "content=" + response.body().toString());
            }

            @Override
            public void onFailure(Call<BaseBean<UserInfo>> call, Throwable t) {
                if (t != null)
                    Log.e("debug", "errror=" + t.toString());
                else {
                    Log.e("debug", "errror=");
                }
            }
        });
    }
}
