package zero.retrofit;

/**
 * Created by tengtao on 16/8/18.
 */
public class LoginRequestParams {

    private String cMob;
    private String cPass;
    private String deviceType="1";
    private String deviceId="123";

    public LoginRequestParams(String cMob, String cPass) {
        this.cMob = cMob;
        this.cPass = cPass;
    }

    public String getcMob() {
        return cMob;
    }

    public void setcMob(String cMob) {
        this.cMob = cMob;
    }

    public String getcPass() {
        return cPass;
    }

    public void setcPass(String cPass) {
        this.cPass = cPass;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}

