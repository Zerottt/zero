package zero.retrofit.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tengtao on 16/8/18.
 */
public class UserInfo implements Parcelable {
    @SerializedName("cuserid")
    private String userId;
    private String token;

    public String getCuserid() {
        return userId;
    }

    public void setCuserid(String cuserid) {
        this.userId = cuserid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "userId='" + userId + '\'' +
                ", token='" + token + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.userId);
        dest.writeString(this.token);
    }

    public UserInfo() {
    }

    protected UserInfo(Parcel in) {
        this.userId = in.readString();
        this.token = in.readString();
    }

    public static final Parcelable.Creator<UserInfo> CREATOR = new Parcelable.Creator<UserInfo>() {
        @Override
        public UserInfo createFromParcel(Parcel source) {
            return new UserInfo(source);
        }

        @Override
        public UserInfo[] newArray(int size) {
            return new UserInfo[size];
        }
    };
}
