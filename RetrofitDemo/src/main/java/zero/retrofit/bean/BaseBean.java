package zero.retrofit.bean;

/**
 * Created by tengtao on 16/8/18.
 */
public class BaseBean<T> {
    private String error;
    private String information;
    private T datas;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public T getDatas() {
        return datas;
    }

    public void setDatas(T datas) {
        this.datas = datas;
    }

    @Override
    public String toString() {
        return "BaseBean{" +
                "error='" + error + '\'' +
                ", information='" + information + '\'' +
                ", datas=" + datas.toString() +
                '}';
    }
}
